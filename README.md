#   Myaderm - READ ME

##  Round 2 updates

### Questions
    1.  Is easylockdown being used?
    2.  is BoldScript being used?
    
### Update List
    1.  Tightened up Yotpo reviews on PDP
    2.  Removed duplicate versions of jQuery
    3.  Removed unused JS from theme.liquid
        - Added to separate files for safe keeping
    4.  Unused files removed:
        - assets/style.css
        - sections/nomoreblemish-control.liquid
        - sections/top-bar.liquid
        - snippets/bk-tracking.liquid
        - snippets/easylockdown_body.liquid
        - snippets/easylockdown_head.liquid
        - templates/page.nomoreblemish.liquid
    5.  Moved currency selector code to snippets/currency-selector.liquid for consistency
    6.  Moved all header JS to custom-theme.js.liquid for loading consistency
    7.  Moved all homepage slider JS to custom-theme.js.liquid for loading consistency
    
#### Further recommendations:
    1.  I have yet to resolve XMLHttpRequest warning
    2.  Navigation is being styled with JS classes instead of proper CSS Media Queries.  This should be adjusted and
     refined to properly load on the page independent of the JS classes.
    3.  Shopify loading image should be removed; currently left because flash of unstyled navigation on page load due
     to aforementioned navigation issue
    4.  Further cleanup/consolidation of JS & CSS for site optimization & pagespeed

-----

##  Completed:
- Formatted & removed unnecessary code from `theme.liquid`
- Moved relevant CSS & JS in `theme.liquid` to assets folder (`custom.scss.liquid` & `custom-theme.js.liquid`)
- Encapsulated JS for Product Gallery to only load on Product Page (corrected JS error)
- Removed duplicate calls for homepage & unused sliders (corrected 3 JS errors)


##  Audit:
- `[Deprecation] Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the
 end user's experience. For more help, check https://xhr.spec.whatwg.org/.`
    - This is only a warning, but it seems to trigger upon every page load.  I think this might have to do with
     incorrect ajax loading tied to the shopify icon that displays upon every page load.
- Tighten up Yotpo container to have a max-width on Product page
- Clean up unnecessary templates